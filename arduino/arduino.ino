#include <Adafruit_NeoPixel.h>
#define PIXELS_PIN 6
#define NUM_PIXELS 71

Adafruit_NeoPixel pixels(NUM_PIXELS, PIXELS_PIN, NEO_GRB + NEO_KHZ800);

// Maximum power that USB can prowide for 142 neopixels (1m) is 100. (About 2A.)
//*
int maxRed = 100;
int maxGreen = 100;
int maxBlue = 100;
/*/
// TEST VALUES
int maxRed = 20;
int maxGreen = 20;
int maxBlue = 20;
//*/
int brightness = 0;

void (*runningAnimation)();
unsigned long lastAnimationRun = 0;
unsigned long lastAnimationStart = 0;

void startAnimation(void* animation) {
  lastAnimationStart = millis();
  runningAnimation = animation;
}

uint32_t getColor(int customBrightness) {
  return pixels.Color(
    (maxRed   * customBrightness) / 100,
    (maxGreen * customBrightness) / 100,
    (maxBlue  * customBrightness) / 100
  );
}

uint32_t getColor() {
  return getColor(brightness);
}

void directPixelsUpdate() {
  for (int i = 0; i < pixels.numPixels(); i++) {
    pixels.setPixelColor(i, getColor());
  }
  pixels.show();
}

void basicAliveAnimation() {
  unsigned long mil = millis();
  int pos = (( mil / 40 ) % (NUM_PIXELS + 20) - 10);
  for (int i = 0; i < pixels.numPixels(); i++) {
    int b = (brightness *  abs(i - pos)) / 20;
    if (b > brightness) {b = brightness; };
    pixels.setPixelColor(i, getColor(b));
  }
  pixels.show();
}

void OffAnimation() {
  unsigned long mil = millis();
  int pos = (mil - lastAnimationStart) / 50;
  for (int i = 0; i < pixels.numPixels(); i++) {
    int b = (brightness * (i-pos) / 10);
    if (b > brightness) {b = brightness; };
    if (b < 0) {b = 0; };
    pixels.setPixelColor(i, getColor(b));
  }
  pixels.show();
  if (pos >= pixels.numPixels() + 16) {
      startAnimation(NULL);
      brightness = 0;
  }
}

void OnAnimation() {
  unsigned long mil = millis();
  int pos = (mil - lastAnimationStart) / 50;
  for (int i = 0; i < pixels.numPixels(); i++) {
    int b = (brightness * (pos-i) / 10);
    if (b > brightness) {b = brightness; };
    if (b < 0) {b = 0; };
    pixels.setPixelColor(i, getColor(b));
  }
  pixels.show();
  if (pos >= pixels.numPixels() + 16) {
      startAnimation(NULL);
  }
}

void setup() {
  runningAnimation = NULL;
  // put your setup code here, to run once:
  // pinMode(LED_BUILTIN, OUTPUT); // Init LED
  // digitalWrite(LED_BUILTIN, LOW); // Turn LED off
  Serial.begin(9600); // Init Serial port
  pixels.begin();  // Init pixels
  Serial.println("event:booted");  // Sending info to app
}

void loop() {
  if (runningAnimation != NULL) {
    unsigned long mil = millis();
    if ((mil % 50 == 0) && (mil > lastAnimationRun + 49)) {
      runningAnimation();
      lastAnimationRun = mil;
    }
  }
}

void eventSwitch(String event) {
    if (event == "set:on") {
      // digitalWrite(LED_BUILTIN, HIGH);
      brightness = 100;
      // directPixelsUpdate();
      startAnimation(&OnAnimation);
      Serial.println("light:on");
    }
    if (event == "set:off") {
      // digitalWrite(LED_BUILTIN, LOW);
      // brightness = 0;
      //directPixelsUpdate();
      startAnimation(&OffAnimation);
      Serial.println("light:off");
    }
    if (event == "set:brightness.25") {
      // digitalWrite(LED_BUILTIN, LOW);
      brightness = 25;
      directPixelsUpdate();
      Serial.println("light:brightness.25");
    }
    if (event == "set:brightness.50") {
      // digitalWrite(LED_BUILTIN, LOW);
      brightness = 50;
      directPixelsUpdate();
      Serial.println("light:brightness.50");
    }
    if (event == "set:brightness.75") {
      // digitalWrite(LED_BUILTIN, LOW);
      brightness = 75;
      directPixelsUpdate();
      Serial.println("light:brightness.75");
    }
    if (event == "animation:basic") {
      startAnimation(&basicAliveAnimation);
      Serial.println("animation:basic");
    }
    if (event == "animation:stop") {
      startAnimation(NULL);
      Serial.println("animation:stop");
    }
    if (event == "check:alive") { Serial.println("base:alive");}
    if (event == "check:brightness") { Serial.print("light:brightness."); Serial.print(brightness, DEC); Serial.println("");}
}


String inputString = "";   
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      eventSwitch(inputString);
      inputString = "";
      return;
    }
    inputString += inChar;
  }
}
