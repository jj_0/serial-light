# Serial Light

Goal is to create light that can be put along monitors to create nice ambient light autmatized via PC. 

## Discontinued project ❌
I decided to end this project, because I replaced it by [InSaneFactory](https://gitlab.com/insanefactory). ISF is modular and more universal system, created mainly for keyboard, but can work for many more, monitor lights included.

## Used tutorials
* https://www.electronjs.org/docs/v14-x-y/tutorial/quick-start
* https://serialport.io/docs/guide-usage
* https://www.electronjs.org/docs/latest/api/tray
* https://github.com/serialport/electron-serialport
* https://www.npmjs.com/package/suncalc
* https://www.electronjs.org/docs/latest/api/global-shortcut

---
---
---
