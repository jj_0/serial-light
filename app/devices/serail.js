const SerialPort = require('serialport');

// export class SerialDevice {
class SerialDevice {
    constructor(port, name) {
        this.name = name;
        this.portPath = port
        this.state = {
            "brightness": false,
            "livenessChecker": false,
            "alive": false,
        }
    }

    openPort() {
        this.port = new SerialPort(this.portPath);
        port.on('data', data => {
            console.log("DATA from ", this.name, data.toString());
            recivedString = data.toString().replace("\n", "").replace("\r", "");
            let [deviceEvent, deviceData] = recivedString.split(":");
            switch(deviceEvent) {
                case "light":
                    switch (deviceData) {
                        case "on": this.state.brightness = 100; break;
                        case "off": this.state.brightness = 0; break;
                    }
                    break;
                case "event":
                    switch (deviceData) {
                        case "booted": this.state.livenessChecker = true; tray.setTitle("Booted"); break;
                    }
                    break;
                case "base":
                    if (deviceData == "alive") {
                        this.state.livenessChecker = true;
                    }
                    break;
            }
        });
    }

    reopenPort() {
        delete(this.port);
        this.openPort();
    }

    aliveCheckSend() {
        this.state.livenessChecker = false;
        this.port.write("check:alive\n");
    }

    get power() {
        return this.state.brightness > 0;
    }

    setOff() {
        self.port.write("set:off\n");
    }

    setOn() {
        self.port.write("set:on\n");
    }
}

