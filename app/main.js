const { app, BrowserWindow, Menu, Tray, powerMonitor, globalShortcut } = require('electron');
const SerialPort = require('serialport');
const SunCalc = require('suncalc');
const SerialDevice = require('./devices/serail');
const Store = require('electron-store');

// GLOBALS
let tray = null;
let port = null;
let lightState = {
    "livenessChecker": false,
    "alive": false,
    "power": false 
}
let systemStatus = {
    "locked": false,
    "manualySet": false,
    "manualySetTo": false,
};

let devices = []

PORT_ADDRESS = '/dev/tty.usbserial-141240';

const defaultConfig = {
        "position": {
            "lat": 49.2,
            "lon": 16.58
        },
        "devices": [
            {
                "type": "serial",
                "ports": [],
                "name": "Default"
            }
        ]
}

const config = new Store({'defaults': defaultConfig});
console.log("Config file at ", config.path);

// function createWindow () {
//     const win = new BrowserWindow({
//         width: 400,
//         height: 400,
//         resizable: false
//     })
//     win.loadFile('index.html')
// }
// app.whenReady().then(() => {
//     createWindow()
// })


// Tray icon disapreas:
// https://stackoverflow.com/questions/58594357/nodejs-electron-tray-icon-disappearing-after-a-minute

function isDark() {
    let date = new Date();
    let times = SunCalc.getTimes(date, config.store.position.lat, config.store.position.lon);
    return (times.sunriseEnd > date) || (times.sunsetStart < date);
}

function update() {
    lightState.alive = lightState.livenessChecker;
    if (lightState.alive) {
        tray.setTitle("");
    } else {
        tray.setTitle("Unpluged");
    }
    lightState.livenessChecker = false;
    if (port) {
        if (port.isOpen) {
            port.write("check:alive\n");
        } else {
            let deadPort = port;
            SerialPort.list().then(console.log);
            initSerialPort();
            delete(deadPort);
        }
    }
    if (lightState.alive) {
        if (systemStatus.locked) {
            if (lightState.power) {
                port.write("set:off\n");
            }
        } else {
            if (lightState.power != isDark()) {
                if (lightState.power) {
                    port.write("set:off\n");
                } else {
                    port.write("set:on\n");
                }
            }
        }
    }
}

function initTray() {
    tray = new Tray('icons/tray.png', "SL")
    tray.addListener("click", () => {
        console.log("Clicked tray!");
        if (lightState.alive) {
            console.log("Sending command!");
            if (lightState.power) {
                port.write("set:off\n");
            } else {
                port.write("set:on\n");
            }
        }
    });

    tray.setTitle("Init...");
    setInterval(update, 30000)
}

function initSerialPort() {
    port = new SerialPort(PORT_ADDRESS)
    port.on('data', data => {
        console.log("DATA: ", data.toString());
        let messages = data.toString().split("\n");
        for (let mi in messages) {
            mess = messages[mi];
            recivedString = mess.replace("\n", "").replace("\r", "");
            let [deviceEvent, deviceData] = recivedString.split(":");
            switch(deviceEvent) {
                case "light":
                    switch (deviceData) {
                        case "on": lightState.power = true; break;
                        case "off": lightState.power = false; break;
                    }
                    break;
                case "event":
                    switch (deviceData) {
                        case "booted": lightState.livenessChecker = true; tray.setTitle("Booted"); update(); break;
                    }
                    ;
                    break;
                case "base":
                    // console.log(deviceData, deviceEvent);
                    if (deviceData == "alive") {
                        lightState.livenessChecker = true;
                    }
                    break;
            }
        }
    })

    port.write("check:alive\n");
}

powerMonitor.on("lock-screen", d => {
    systemStatus.locked = true;
    update();
})


powerMonitor.on("unlock-screen", d => {
    systemStatus.locked = false;
    update();
})

app.whenReady().then(() => {
    initTray();
    initSerialPort();
})
